package gui.admin.roomextra;

import java.util.Optional;

import gui.admin.template.DeleteOperation;
import hotelmaster.exceptions.PriceDescriberRemovalException;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

/**
 * 
 * here the admin can delete a specific room extra.
 *
 */
public class CancellaSupplementoExtra extends DeleteOperation {
    /**
     * 
     * @param testo
     *            is the text of the label
     * @param titolo
     *            is the title of the frame
     */
    public CancellaSupplementoExtra(final String testo, final String titolo) {
        super(testo, titolo);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(RoomExtraPriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendData() throws PriceDescriberRemovalException {
        Optional<RoomExtraPriceDescriber> opt = Hotel.instance().getPriceView(RoomExtraPriceDescriber.class).stream()
                .filter(stay -> stay.getDescription().equals(this.getElementoScelto())).findFirst();
            HotelManager.create().removePriceDescriber(opt.get());
    }
}
