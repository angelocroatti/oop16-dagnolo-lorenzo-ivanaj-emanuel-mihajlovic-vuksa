package gui.admin.roomtype;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import gui.admin.mainview.Scelte;
import gui.admin.rooms.SingolaModificaExtraCamera;
import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;
import hotelmaster.structure.Room;
import hotelmaster.structure.RoomTemplate;

/**
 * 
 * here the admin can modify the type of a specific room.
 *
 */
public class ModificaTipoCamera {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private JComboBox<String> descrizione;
    private JLabel labelDescrizione;
    private JButton conferma;
    private JButton annulla;
    private GridBagConstraints grid;
    private Dimension screenSize;
    private Image okIcon;
    private Image backIcon;
    private String[] str;
    private String rispSupp;
    private RoomTemplate template;

    // CHECKSTYLE:OFF: MagicNumber
    /**
     * 
     * @param room
     * @param template
     */
    public ModificaTipoCamera(final Room room, final RoomTemplate template) {
        this.template = template;
        this.frame = new JFrame("Tipo Camera");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setSize(new Dimension(600, 600));
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        Hotel.instance().getPriceView(RoomTypePriceDescriber.class)
                .forEach(type -> System.out.println(type.getDescription()));
        this.str = Hotel.instance().getPriceView(RoomTypePriceDescriber.class).stream()
                .map(type -> type.getDescription()).collect(Collectors.toList())
                .toArray(new String[Hotel.instance().getPriceView(RoomTypePriceDescriber.class).size()]);
        this.panel = new JPanel(new GridBagLayout());
        this.grid = new GridBagConstraints();
        this.panel.setBackground(Color.CYAN);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.cyan);
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/add.png")).getImage();
        this.backIcon = new ImageIcon(this.getClass().getResource("/icons/back.png")).getImage();
        this.conferma = new JButton("");
        this.annulla = new JButton("");
        this.conferma.setIcon(new ImageIcon(this.okIcon));
        this.annulla.setIcon(new ImageIcon(this.backIcon));
        this.grid = new GridBagConstraints();
        this.grid.gridx = 0;
        this.grid.gridy = 0;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.grid.gridx = 1;
        this.grid.gridy = 0;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.labelDescrizione = new JLabel("Scegli tipo di camera");
        this.grid.gridx = 0;
        this.grid.gridy = 1;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.panel.add(this.labelDescrizione, this.grid);
        this.descrizione = new JComboBox<>(this.str);
        this.descrizione.setBackground(Color.yellow);
        this.grid.gridx = 1;
        this.grid.gridy = 1;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.panel.add(this.descrizione, this.grid);
        this.conferma.addActionListener(b -> {
            try {
                int risp = JOptionPane.showConfirmDialog(this.frame, "Confermare?", "Conferma", JOptionPane.YES_OPTION);
                if (risp == JOptionPane.YES_OPTION) {
                    this.rispSupp = this.descrizione.getSelectedItem().toString();
                    try {
                        this.template.setRoomType(Hotel.instance().getPriceView(RoomTypePriceDescriber.class).stream()
                                .filter(sogg -> sogg.getDescription().equals(this.rispSupp)).findFirst().get());
                        HotelManager.create().updateRoom(room, template);
                    } catch (MissingEntityException e) {
                        e.printStackTrace();
                    }
                    this.frame.setVisible(false);
                    this.frame.dispose();
                    new SingolaModificaExtraCamera();
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(frame, "Attualmente questa camera non può essere modificata", "Errore",
                        JOptionPane.ERROR_MESSAGE);

            }
        });
        this.annulla.addActionListener(a -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Scelte();
        });
        this.southPanel.add(this.annulla);
        this.southPanel.add(this.conferma);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.panel);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }

}