package gui.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import gui.admin.mainview.Scelte;
import hotelmaster.database.utilities.StatisticUtilitiesImpl;
import net.proteanit.sql.DbUtils;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * here the admin can show the statics.
 *
 */
public class Storico extends JFrame {
    private static final long serialVersionUID = 1L;
    private JButton esci;
    private JPanel southPanel;

    /**
     * 
     */
    public Storico() {
        this.southPanel = new JPanel();
        this.esci = new JButton();
        this.esci.setIcon(new ImageIcon(new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage()));
        this.esci.addActionListener(e -> {
            setVisible(false);
            dispose();
            new Scelte();
        });
        this.southPanel.add(esci);
        setLocation(400, 100);

        final JTable table = new JTable() {
            public boolean isCellEditable(final int row, final int column) {
                return false;
            };

            private static final long serialVersionUID = 1L;
        };
        table.setRowHeight(40);
        table.setBackground(Color.white);
        table.setModel(DbUtils.resultSetToTableModel(new StatisticUtilitiesImpl().getOldStays().get()));
        JScrollPane sp = new JScrollPane(table);
        sp.setPreferredSize(new Dimension(300, 100));
        getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        getContentPane().add(sp, BorderLayout.CENTER);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        pack();

        setVisible(true);
    }
}