package gui.admin.prices;

import java.util.Optional;

import gui.admin.template.PriceOperation;
import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;
//CHECKSTYLE:OFF: MagicNumber

/**
 * 
 * here the admin can modify the price a specific person.
 *
 */
public class PricePerson extends PriceOperation {
    /**
     * 
     * @param is
     *            the text of a label that will be in PriceOperation
     * 
     */
    public PricePerson(String descrizione) {
        super(descrizione);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(PersonPriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendPrice(Double price, String name) {
        Optional<PersonPriceDescriber> opt = Hotel.instance().getPriceView(PersonPriceDescriber.class).stream()
                .filter(type -> type.getDescription().equals(name)).findFirst();
        HotelManager.create().setPriceDescriber(opt.get(), price);
    }

}
