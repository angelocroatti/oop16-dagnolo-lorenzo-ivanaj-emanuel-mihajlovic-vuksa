package gui.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.toedter.calendar.JDateChooser;

import gui.admin.mainview.Scelte;
import hotelmaster.database.utilities.StatisticUtilities;
import hotelmaster.database.utilities.StatisticUtilitiesImpl;
import hotelmaster.utility.time.FixedPeriod;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * Statistics for admin.
 *
 */
public class Statistica {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private JDateChooser da;
    private JDateChooser a;
    private JLabel labelDa;
    private JLabel labelA;
    private DateFormat nuovogg;
    private DateFormat nuovomm;
    private DateFormat nuovoaa;
    private Integer ggDa;
    private Integer mmDa;
    private Integer aaDa;
    private Integer ggA;
    private Integer mmA;
    private Integer aaA;
    private LocalDate localDa;
    private LocalDate localA;
    private Dimension screenSize;
    private JButton conferma;
    private JButton esci;
    private Image okIcon;
    private Image exitIcon;
    private JPanel labelPanel;
    private Double valoreTotale;
    private final StatisticUtilities staticsDay;
    private FixedPeriod fp;
    private JLabel labelTotale;

    /**
     * 
     */
    public Statistica() {
        this.frame = new JFrame("Ricavo totale ottenuto");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setSize(new Dimension(500, 400));
        this.nuovogg = new SimpleDateFormat("dd");
        this.nuovomm = new SimpleDateFormat("MM");
        this.nuovoaa = new SimpleDateFormat("yyyy");
        this.staticsDay = new StatisticUtilitiesImpl();
        this.panel = new JPanel();
        this.panel.setBackground(Color.cyan);
        this.labelPanel = new JPanel();
        this.labelPanel.setBackground(Color.CYAN);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.cyan);
        this.labelPanel.setVisible(false);
        Font font = new Font("Baskerville", Font.HANGING_BASELINE, 90);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((this.screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/cash.png")).getImage();
        this.exitIcon = new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage();
        this.conferma = new JButton("");
        this.conferma.setIcon(new ImageIcon(this.okIcon));
        this.esci = new JButton("");
        this.esci.setIcon(new ImageIcon(this.exitIcon));
        this.labelDa = new JLabel("Da");
        this.da = new JDateChooser();
        this.da.setDateFormatString("dd/mm/aaaa");
        this.da.setLocale(Locale.ITALY);
        this.da.setDateFormatString("dd/MM/yyyy");
        this.da.setFont(new Font("Tahoma", Font.PLAIN, 15));
        this.da.setPreferredSize(new Dimension(130, 20));
        this.labelA = new JLabel("A");
        this.a = new JDateChooser();
        this.a.setDateFormatString("dd/mm/aaaa");
        this.a.setLocale(Locale.ITALY);
        this.a.setDateFormatString("dd/MM/yyyy");
        this.a.setFont(new Font("Tahoma", Font.PLAIN, 15));
        this.a.setPreferredSize(new Dimension(130, 20));
        this.conferma.addActionListener(e -> {
            this.labelPanel.removeAll();
            this.labelPanel.revalidate();
            this.labelPanel.repaint();
            try { 
                this.ggDa = Integer.parseInt(this.nuovogg.format(this.da.getDate()));
                this.mmDa = Integer.parseInt(this.nuovomm.format(this.da.getDate()));
                this.aaDa = Integer.parseInt(this.nuovoaa.format(this.da.getDate()));
                this.ggA = Integer.parseInt(this.nuovogg.format(this.a.getDate()));
                this.mmA = Integer.parseInt(this.nuovomm.format(this.a.getDate()));
                this.aaA = Integer.parseInt(this.nuovoaa.format(this.a.getDate()));
                this.localDa = LocalDate.of(this.aaDa, Month.of(this.mmDa), this.ggDa);
                this.localA = LocalDate.of(this.aaA, Month.of(this.mmA), this.ggA);
                this.fp = FixedPeriod.of(this.localDa, this.localA);
                this.valoreTotale = this.staticsDay.getTotalEarnings(this.fp);
                this.labelPanel.setVisible(true);
                this.labelTotale = new JLabel(Double.toString(this.valoreTotale) + "€");
                this.labelTotale.setFont(font);
                this.labelPanel.add(this.labelTotale);
            } catch (IllegalArgumentException e1) {
                JOptionPane.showMessageDialog(this.frame, "Errore inserimento data", "Errore data",
                        JOptionPane.ERROR_MESSAGE);
                System.out.println(this.localDa);
                System.out.println(this.localA);
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(this.frame, "Errore inserimento data");
            }
        });
        this.esci.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Scelte();
        });
        this.southPanel.add(this.esci);
        this.panel.add(this.labelDa);
        this.panel.add(this.da);
        this.panel.add(this.labelA);
        this.panel.add(this.a);
        this.panel.add(this.conferma);
        this.frame.getContentPane().add(this.labelPanel);
        this.frame.getContentPane().add(this.panel, BorderLayout.NORTH);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);

    }
}
