package gui.admin.stayextra;

import java.util.Optional;

import gui.admin.template.DeleteOperation;
import hotelmaster.exceptions.PriceDescriberRemovalException;
import hotelmaster.pricing.StayExtraPriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

/**
 * 
 * here the admin can delete specific stay extra.
 *
 */
public class CancellaSupplementoSoggiorno extends DeleteOperation {
    /**
     * 
     * @param testo
     *            is the text of the label
     * @param titolo
     *            is the title of the frame
     */
    public CancellaSupplementoSoggiorno(final String testo, final String titolo) {
        super(testo, titolo);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(StayExtraPriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendData() throws PriceDescriberRemovalException {
        Optional<StayExtraPriceDescriber> opt = Hotel.instance().getPriceView(StayExtraPriceDescriber.class).stream()
                .filter(stay -> stay.getDescription().equals(this.getElementoScelto())).findFirst();
        HotelManager.create().removePriceDescriber(opt.get());
    }
}
