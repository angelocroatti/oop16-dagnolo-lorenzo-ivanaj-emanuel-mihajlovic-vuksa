package gui.admin.rooms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import gui.admin.mainview.Scelte;
import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;
import hotelmaster.structure.RoomTemplate;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * here the admin can choose the type of a room/rooms that he want to create.
 *
 */
public class TipoCamera {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private Dimension screenSize;
    private Integer numCamere;
    private Integer piano;
    private JButton conferma;
    private JButton annulla;
    private JButton esci;
    private Image okIcon;
    private Image backIcon;
    private Image exitIcon;
    private Integer piani;
    private Integer camere;
    private RoomTemplate room;
    private JLabel label;
    private GridBagConstraints grid;
    private JComboBox<String> combo;
    private String[] array;
    private Map<JCheckBox, RoomTypePriceDescriber> listBox;

    /**
     * 
     * @param piano
     *            is the number of floors
     * @param camera
     *            is the number room/rooms
     * @param rooms
     *            is a {@link RoomTemplate}
     */
    public TipoCamera(final int piano, final int camera, final RoomTemplate rooms) {
        this.frame = new JFrame();
        this.frame.setSize(new Dimension(400, 300));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.array = Hotel.instance().getPriceView(RoomTypePriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        this.listBox = new HashMap<>();
        this.piani = piano;
        this.camere = camera;
        this.room = rooms;
        this.panel = new JPanel(new GridBagLayout());
        this.grid = new GridBagConstraints();
        this.panel.setBackground(Color.cyan);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        this.numCamere = camere;
        this.piano = piani;
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/add.png")).getImage();
        this.backIcon = new ImageIcon(this.getClass().getResource("/icons/back.png")).getImage();
        this.exitIcon = new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage();
        this.label = new JLabel("Scegli tipo di camera");
        this.grid.gridx = 0;
        this.grid.gridy = 0;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.panel.add(this.label, this.grid);
        this.combo = new JComboBox<>(this.array);
        this.combo.setBackground(Color.YELLOW);
        this.grid.gridx = 1;
        this.grid.gridy = 0;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.panel.add(this.combo, this.grid);
        this.conferma = new JButton("");
        this.conferma.setIcon(new ImageIcon(this.okIcon));
        this.annulla = new JButton("");
        this.annulla.setIcon(new ImageIcon(this.backIcon));
        this.esci = new JButton("");
        this.esci.setIcon(new ImageIcon(this.exitIcon));
        this.conferma.addActionListener(a -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Confermare?", "Conferma", JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                try {
                    this.room.setRoomType(Hotel.instance().getPriceView(RoomTypePriceDescriber.class).stream()
                            .filter(type -> type.getDescription().equals(combo.getSelectedItem().toString()))
                            .findFirst().get());

                    final JDialog dialog = new JDialog(this.frame);
                    final JOptionPane optionPane = new JOptionPane("Creazione camere in corso, attendere prego...",
                            JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[] {}, null);
                    dialog.setTitle("Creazione camere");
                    dialog.setModal(true);

                    dialog.setContentPane(optionPane);

                    dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                    dialog.pack();
                    dialog.setLocationRelativeTo(this.frame);
                    dialog.setResizable(false);
                    SwingUtilities.invokeLater(() -> {
                        try {
                            HotelManager.create().addRooms(this.piano, this.numCamere, this.room);
                        } catch (MissingEntityException e) {
                            e.printStackTrace();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        dialog.dispose();
                    });
                    dialog.setVisible(true);
                } catch (NoSuchElementException e) {
                    JOptionPane.showMessageDialog(frame, "Hai inserito un numero di camera o piano sbagliato",
                            "Errore creazione supplemento camera", JOptionPane.ERROR_MESSAGE);
                } catch (MissingEntityException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(frame, "Hai inserito un numero di camera o piano sbagliato",
                            "Errore creazione supplemento camera", JOptionPane.ERROR_MESSAGE);
                } finally {
                    this.frame.setVisible(false);
                    this.frame.dispose();
                    new CreaCamera();
                }
            }
        });
        this.annulla.addActionListener(b ->

        {
            new AggiuntaInterni(this.numCamere, this.piano);
            this.frame.setVisible(false);
            this.frame.dispose();
        });
        this.esci.addActionListener(c -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Scelte();
        });
        this.southPanel.add(this.annulla);
        this.southPanel.add(this.esci);
        this.southPanel.add(this.conferma);
        this.frame.getContentPane().add(this.panel);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }

    public static void main(final String[] args) {
        new TipoCamera(1, 1, RoomTemplate.create());
    }
}
