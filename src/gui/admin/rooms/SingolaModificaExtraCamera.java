package gui.admin.rooms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.admin.mainview.Scelte;
import gui.admin.roomtype.ModificaTipoCamera;
import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.Room;
import hotelmaster.structure.RoomTemplate;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * here the admin can modify the extra in a specific single room.
 *
 */
public class SingolaModificaExtraCamera {
    private JFrame frame;
    private JPanel panel;
    private JPanel northPanel;
    private JPanel southPanel;
    private JTextField text;
    private JTextField text2;
    private JLabel label;
    private JLabel label2;
    private JButton conferma;
    private JButton completa;
    private JButton esci;
    private Dimension screenSize;
    private Image okIcon;
    private Image exitIcon;
    private List<JCheckBox> checkBox;
    private Room room;

    /**
     * 
     * 
     */
    public SingolaModificaExtraCamera() {
        this.frame = new JFrame("Modifica singola camera");
        this.frame.setSize(800, 600);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.panel = new JPanel();
        this.panel.setBackground(Color.cyan);
        this.panel.setVisible(false);
        this.northPanel = new JPanel();
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        this.northPanel.setBackground(Color.CYAN);
        this.label = new JLabel("Inserisci numero della camera");
        this.text = new JTextField(3);
        this.text.setBackground(Color.YELLOW);
        this.label2 = new JLabel("Inserisci il numero del piano");
        this.text2 = new JTextField(3);
        this.text2.setBackground(Color.yellow);
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/ok.png")).getImage();
        this.exitIcon = new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage();
        this.conferma = new JButton("Conferma");
        this.completa = new JButton("");
        this.completa.setIcon(new ImageIcon(this.okIcon));
        this.completa.setEnabled(false);
        this.esci = new JButton("");
        this.esci.setIcon(new ImageIcon(this.exitIcon));
        this.conferma.addActionListener(b -> {
            try {
                int floor = Integer.parseInt(this.text2.getText().toString());
                int number = Integer.parseInt(this.text.getText().toString());
                if (this.text.getText().length() == 0 || this.text2.getText().length() == 0) {
                    JOptionPane.showMessageDialog(frame, "Riprova", "Errore", JOptionPane.ERROR_MESSAGE);
                } else {
                    int risp = JOptionPane.showConfirmDialog(this.frame,
                            "Vuoi modificare la camera " + number +" al piano " + floor + " ?", "Conferma", JOptionPane.YES_OPTION);
                    if (risp == JOptionPane.YES_OPTION) {
                        Optional<Room> foundRoom = Hotel.instance().getRoomView().stream().filter(
                                room -> room.getID().getFloor() == floor && room.getID().getNumberOnFloor() == number)
                                .findFirst();
                        if (!foundRoom.isPresent()) {
                            JOptionPane.showMessageDialog(frame, "La camera non esiste", "Errore",
                                    JOptionPane.ERROR_MESSAGE);
                        } else {
                            this.completa.setEnabled(true);
                            this.panel.setVisible(true);
                            this.room = foundRoom.get();
                            this.checkBox = Hotel.instance().getPriceView(RoomExtraPriceDescriber.class).stream()
                                    .map((roomExtra) -> {
                                        JCheckBox tmp = new JCheckBox(roomExtra.getDescription());
                                        this.panel.add(tmp);
                                        tmp.setSelected(this.room.getExtrasView().contains(roomExtra));
                                        tmp.setBackground(Color.CYAN);
                                        return tmp;
                                    }).collect(Collectors.toList());
                        }
                    }
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(frame, "Errore inserimento", "Errore", JOptionPane.ERROR_MESSAGE);
            }
        });
        this.completa.addActionListener(c -> {
            int risp = JOptionPane.showConfirmDialog(this.frame,
                    "Vuoi confermare le modifiche della camera ?", "Conferma",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                RoomTemplate template = RoomTemplate.create();
                for (JCheckBox extra : checkBox) {
                    if (extra.isSelected()) {
                        try {
                            template.addRoomExtra(Hotel.instance().getPriceView(RoomExtraPriceDescriber.class).stream()
                                    .filter(other -> other.getDescription().equals(extra.getText().toString()))
                                    .findFirst().get());
                        } catch (MissingEntityException e) {
                            e.printStackTrace();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }
                }
                this.frame.setVisible(false);
                this.frame.dispose();
                new ModificaTipoCamera(this.room, template);
            }
        });
        this.esci.addActionListener(c -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Vuoi confermare di uscire ?", "Conferma",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.frame.setVisible(false);
                this.frame.dispose();
                new Scelte();
            }
        });
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.northPanel.add(this.label);
        this.northPanel.add(this.text);
        this.northPanel.add(this.label2);
        this.northPanel.add(this.text2);
        this.northPanel.add(this.conferma);
        this.southPanel.add(this.esci);
        this.southPanel.add(this.completa);
        this.frame.getContentPane().add(this.northPanel, BorderLayout.NORTH);
        this.frame.getContentPane().add(this.panel);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }
}