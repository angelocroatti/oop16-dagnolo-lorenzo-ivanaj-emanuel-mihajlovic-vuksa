package gui.admin.rooms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import gui.admin.mainview.Cancella;
import gui.admin.mainview.Scelte;
import hotelmaster.exceptions.RoomRemovalException;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;
import hotelmaster.structure.Room;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * here the admin can delete a specific room.
 *
 */
public class CancellaCamera {
    private JFrame frame;
    private List<JButton> button;
    private JButton indietro;
    private JButton esci;
    private JButton conferma;
    private JComboBox<Integer> piani;
    private JPanel pannelloFiltri;
    private JPanel pannelloBottoni;
    private JPanel southPanel;
    private JLabel label;
    private Integer[] strPiani;
    private List<String> listaSelezione;

    /**
     * 
     */
    public CancellaCamera() {
        this.frame = new JFrame("Hotel Master");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pannelloFiltri = new JPanel(new FlowLayout(FlowLayout.LEFT));
        this.pannelloBottoni = new JPanel(new GridLayout(15, 10));
        this.southPanel = new JPanel();
        this.button = new ArrayList<>();
        this.listaSelezione = new ArrayList<>();
        for (final Room stanza : Hotel.instance().getRoomView()) {
            JButton b = new JButton(stanza.getID().getFullID());
            button.add(b);
            if (!stanza.hasOccupations()) {
                b.addActionListener(a -> {
                    if (!listaSelezione.contains(b.getText())) {
                        this.listaSelezione.add(b.getText());
                    }
                    b.setBackground(Color.GREEN);
                });
            } else {
                b.setBackground(Color.RED);
                b.setEnabled(false);
            }
            this.pannelloBottoni.add(b);
        }
        this.label = new JLabel("Scegli il numero del piano");
        this.strPiani = Hotel.instance().getFloorView().keySet()
                .toArray(new Integer[Hotel.instance().getFloorView().keySet().size()]);
        this.piani = new JComboBox<>(this.strPiani);
        this.esci = new JButton("Esci");
        this.indietro = new JButton("Indietro");
        this.conferma = new JButton("Conferma");
        this.esci.addActionListener(c -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Scelte();
        });
        this.indietro.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Cancella();
        });
        this.conferma.addActionListener(d -> {
            if (!listaSelezione.isEmpty()) {
                int risp = JOptionPane.showConfirmDialog(this.frame,
                        "Sei sicuro di volere cancellare le camere " + this.listaSelezione + " ?", "Conferma",
                        JOptionPane.YES_OPTION);
                if (risp == JOptionPane.YES_OPTION) {
                    for (String stanza : listaSelezione) {
                        Optional<Room> opt = Hotel.instance().getRoomView().stream()
                                .filter(r -> r.getID().getFullID().equals(stanza)).findFirst();
                        if (opt.isPresent()) {
                            try {
                                HotelManager.create().removeRoom(opt.get());
                            } catch (RoomRemovalException | IllegalArgumentException e1) {
                                JOptionPane.showMessageDialog(this.frame,
                                        "Non si può cancellare in questo momento la camera", "Errore momentaneo",
                                        JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                    this.frame.setVisible(false);
                    this.frame.dispose();
                    new CancellaCamera();
                }
            }
        });
        this.pannelloFiltri.add(this.label);
        this.pannelloFiltri.add(this.piani);
        this.southPanel.add(this.indietro);
        this.southPanel.add(this.esci);
        this.southPanel.add(this.conferma);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.pannelloFiltri, BorderLayout.NORTH);
        this.frame.getContentPane().add(this.pannelloBottoni);
        this.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.frame.setVisible(true);
    }
}
