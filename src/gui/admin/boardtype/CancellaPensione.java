package gui.admin.boardtype;

import java.util.Optional;

import gui.admin.template.DeleteOperation;
import hotelmaster.exceptions.PriceDescriberRemovalException;
import hotelmaster.pricing.StayTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

/**
 * 
 * Here the admin can delete one or more board type.
 *
 */
public class CancellaPensione extends DeleteOperation {
    /**
     * 
     * @param testo
     *            is the text of the label
     * @param titolo
     *            is the title of the frame
     */
    public CancellaPensione(final String testo, final String titolo) {
        super(testo, titolo);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(StayTypePriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendData() throws PriceDescriberRemovalException {
        Optional<StayTypePriceDescriber> opt = Hotel.instance().getPriceView(StayTypePriceDescriber.class).stream()
                .filter(stay -> stay.getDescription().equals(this.getElementoScelto())).findFirst();
        HotelManager.create().removePriceDescriber(opt.get());
    }
}