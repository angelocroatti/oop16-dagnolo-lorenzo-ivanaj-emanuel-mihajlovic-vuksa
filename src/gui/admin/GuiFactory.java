package gui.admin;

import java.awt.Color;

import javax.swing.JButton;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * emanu Modify the button, cyan color and setting a text.
 *
 */
public class GuiFactory {
    /**
     * 
     * @param name
     *            the name that will be in button
     * @return the button with cyan color
     */
    public static JButton createButton(final String name) {
        JButton button1 = new JButton(name);
        button1.setBackground(Color.CYAN);
        return button1;
    }
}
