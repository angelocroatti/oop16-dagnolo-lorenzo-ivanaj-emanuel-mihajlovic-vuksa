package gui.admin.mainview;

import java.awt.event.ActionListener;

import gui.admin.account.CancellaAccount;
import gui.admin.boardtype.CancellaPensione;
import gui.admin.floor.RimuoviPiano;
import gui.admin.person.CancellaPersona;
import gui.admin.roomextra.CancellaSupplementoExtra;
import gui.admin.rooms.CancellaCamera;
import gui.admin.roomtype.CancellaTipoCamera;
import gui.admin.season.CancellaStagione;
import gui.admin.stayextra.CancellaSupplementoSoggiorno;
import gui.admin.template.Operations;

/**
 * 
 * Here the admin choose the options for deleting something.
 *
 */
public class Cancella extends Operations {

    @Override
    public ActionListener camera() {

        return (l) -> {
            new CancellaCamera();
            this.shut();
        };
    }

    @Override
    public ActionListener piano() {
        return (l) -> {
            new RimuoviPiano();
            this.shut();
        };
    }

    @Override
    public ActionListener account() {
        return (l) -> {
            new CancellaAccount("Scegli account da cancellare", "Cancella account");
            this.shut();
        };
    }

    @Override
    public ActionListener supplementoSoggiorno() {

        return (l) -> {
            new CancellaSupplementoSoggiorno("Scegli supplemento da cancellare", "Cancellazione supplemento");
            this.shut();
        };
    }

    @Override
    public ActionListener supplementoCamera() {
        return (l) -> {
            new CancellaSupplementoExtra("Scegli extra di una camera da cancellare", "Cancella extra");
            this.shut();
        };
    }

    @Override
    public ActionListener tipoCamera() {
        return (l) -> {
            new CancellaTipoCamera("Scegli tipo camera da cancellare", "Cancella tipo camera");
            this.shut();
        };
    }

    @Override
    public ActionListener tipoPensione() {
        return (l) -> {
            new CancellaPensione("Seleziona tipo di pensione da cancellare", "Cancella pensione");
            this.shut();
        };
    }

    @Override
    public ActionListener stagione() {
        return (l) -> {
            new CancellaStagione("Scegli tipo di stagione da cancellare", "Cancella stagione");
            this.shut();
        };
    }

    @Override
    public ActionListener persone() {
        return (l) -> {
            new CancellaPersona("Scegli persona da cancellare", "Tipo persona da cancellare");
            this.shut();
        };
    }

    @Override
    public ActionListener esci() {
        return (l) -> {
            new Scelte();
            this.shut();
        };
    }
}
