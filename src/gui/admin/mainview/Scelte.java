package gui.admin.mainview;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import gui.admin.GuiFactory;
import gui.admin.account.ModificaAccount;
import gui.admin.rooms.SingolaModificaExtraCamera;
import gui.secretary.LogIn;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * Here the admin can choose an operation from some buttons
 *
 */
public class Scelte {
    private JFrame frame;
    private Dimension screenSize;
    private JPanel panel;
    private JPanel southPanel;
    private JButton crea;
    private JButton cancella;
    private JButton modificaPrezzo;
    private JButton modifica;
    private JButton account;
    private JButton statistica;
    private JButton esci;

    /**
     * 
     */
    public Scelte() {
        this.frame = new JFrame("Hotel Master - Amministratore");
        this.frame.setSize(new Dimension(600, 500));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((this.screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.panel = new JPanel(new GridLayout(3, 2));
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        this.crea = GuiFactory.createButton("Crea");
        this.cancella = GuiFactory.createButton("Cancella");
        this.modificaPrezzo = GuiFactory.createButton("Modifica Prezzo");
        this.modifica = GuiFactory.createButton("Modifica Camera");
        this.account = GuiFactory.createButton("Modifica Account");
        this.statistica = GuiFactory.createButton("Statistiche Hotel");
        this.crea.addActionListener(a -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Crea();
        });
        this.cancella.addActionListener(b -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Cancella();
        });
        this.modifica.addActionListener(c -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new SingolaModificaExtraCamera();
        });
        this.modificaPrezzo.addActionListener(d -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new ModificaPrezzo();
        });
        this.account.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new ModificaAccount();
        });
        this.statistica.addActionListener(f -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Modify();
        });
        this.esci = new JButton("Logout");
        this.esci.setBackground(Color.orange);
        this.esci.addActionListener(e -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Sei sicuro di volere uscire?", "Uscita",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.frame.setVisible(false);
                this.frame.dispose();
                new LogIn();
            }
        });

        this.southPanel.add(this.esci);
        this.panel.add(this.crea);
        this.panel.add(this.cancella);
        this.panel.add(this.modifica);
        this.panel.add(this.modificaPrezzo);
        this.panel.add(this.account);
        this.panel.add(this.statistica);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.panel);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }public static void main(String[] args){
        new Scelte();
    }
}
