package gui.admin.mainview;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import gui.admin.Statistica;
import gui.admin.Storico;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * Here the admin can choose an option from the checkbox.
 *
 */
public class Modify {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private JButton statistiche;
    private JButton storico;
    private JButton indietro;
    private Dimension screenSize;
    private Image backIcon;

    /**
     * 
     */
    public Modify() {
        this.frame = new JFrame("Hotel Master - Statistiche Albergo");
        this.frame.setSize(new Dimension(500, 250));
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.panel = new JPanel(new GridLayout(1, 2));
        this.panel.setBackground(Color.CYAN);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        this.storico = new JButton("Storico");
        this.statistiche = new JButton("Statistiche");
        this.storico.setBackground(Color.cyan);
        this.statistiche.setBackground(Color.cyan);
        this.storico.addActionListener(a -> {
            new Storico();
            this.frame.setVisible(false);
            this.frame.dispose();
        });

        this.statistiche.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Statistica();
        });
        this.panel.add(this.statistiche);
        this.panel.add(this.storico);
        this.backIcon = new ImageIcon(this.getClass().getResource("/icons/back.png")).getImage();
        this.indietro = new JButton("");
        this.indietro.setIcon(new ImageIcon(this.backIcon));
        this.indietro.addActionListener(b -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Scelte();
        });
        this.southPanel.add(this.indietro);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.panel);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }

    public static void main(String[] args) {
        new Modify();
    }
}