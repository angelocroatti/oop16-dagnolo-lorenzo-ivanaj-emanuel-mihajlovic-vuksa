package gui.admin.account;

import gui.admin.template.DeleteOperation;
import hotelmaster.database.admin.AccountManager;
import hotelmaster.database.admin.AccountManagerImpl;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * here, admin, will choose an account to delete.
 *
 */
public class CancellaAccount extends DeleteOperation {

    private AccountManagerImpl accountmanager;
    private String[] array;

    /**
     * 
     * @param testo
     *            is the text of the label
     * @param titolo
     *            is the title of the frame
     */
    public CancellaAccount(final String testo, final String titolo) {
        super(testo, titolo);
    }

    @Override
    public String[] getElements() {
        this.accountmanager = new AccountManagerImpl();
        this.array = this.accountmanager.getAccounts().toArray(new String[this.accountmanager.getAccounts().size()]);
        return this.array;
    }

    @Override
    public void sendData() {
        AccountManager account = new AccountManagerImpl();
        account.removeUserAccount(this.getElementoScelto());
    }
}