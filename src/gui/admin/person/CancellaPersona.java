package gui.admin.person;

import java.util.Optional;

import gui.admin.template.DeleteOperation;
import hotelmaster.exceptions.PriceDescriberRemovalException;
import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * Here the admin can delete a specific person.
 *
 */
public class CancellaPersona extends DeleteOperation {
    /**
     * 
     * @param testo
     *            is the text of label.
     * @param titolo
     *            is the title of frame.
     */
    public CancellaPersona(final String testo, final String titolo) {
        super(testo, titolo);

    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(PersonPriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendData() throws PriceDescriberRemovalException {
        Optional<PersonPriceDescriber> opt = Hotel.instance().getPriceView(PersonPriceDescriber.class).stream()
                .filter(stay -> stay.getDescription().equals(this.getElementoScelto())).findFirst();
        HotelManager.create().removePriceDescriber(opt.get());
    }
}