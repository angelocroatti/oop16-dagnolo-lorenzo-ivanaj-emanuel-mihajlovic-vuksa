package gui.secretary;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import hotelmaster.pricing.StayExtraPriceDescriber;
import hotelmaster.structure.Hotel;

//CHECKSTYLE:OFF
/**
 * 
 * here the secretary can choose the supplement of a room.
 *
 */
public class SceltaSupplementiSoggiorno {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private Dimension screenSize;
    private Map<JLabel, JCheckBox> checkBox = new HashMap<>();
    private JButton ok;
    private JButton indietro;
    private Image backIcon;
    private Image okIcon;

    /**
     * 
     * @param extras
     *            are the extras of a room
     */
    public SceltaSupplementiSoggiorno(final List<String> extras) {
        extras.clear();
        this.frame = new JFrame("Scelta supplementi soggiorno");
        this.frame.setSize(new Dimension(600, 600));
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.panel = new JPanel();
        this.panel.setBackground(Color.cyan);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        Hotel.instance().getPriceView(StayExtraPriceDescriber.class).stream().forEach(e -> {
            JCheckBox tmp = new JCheckBox();
            JLabel label = new JLabel(e.getDescription());
            checkBox.put(label, tmp);
            this.panel.add(label);
            this.panel.add(tmp);
            tmp.setBackground(Color.CYAN);
        });
        this.backIcon = new ImageIcon(this.getClass().getResource("/icons/back.png")).getImage();
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/ok.png")).getImage();
        this.ok = new JButton("");
        this.ok.setIcon(new ImageIcon(this.okIcon));
        this.indietro = new JButton("");
        this.indietro.setIcon(new ImageIcon(this.backIcon));
        this.ok.addActionListener(e -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Confermare?", "Conferma", JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.checkBox.entrySet().stream().forEach(t -> {
                    if (t.getValue().isSelected()) {
                        extras.add(t.getKey().getText());
                    }
                });

                this.frame.setVisible(false);
                this.frame.dispose();

            }
        });
        this.indietro.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
        });
        this.southPanel.add(this.indietro);
        this.southPanel.add(this.ok);
        this.frame.getContentPane().add(this.panel);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setVisible(true);
    }
}