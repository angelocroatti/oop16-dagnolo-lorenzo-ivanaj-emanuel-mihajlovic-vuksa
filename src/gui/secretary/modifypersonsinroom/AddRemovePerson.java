package gui.secretary.modifypersonsinroom;

import gui.secretary.documentamplate.DocumentTemplate;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayState;

/**
 * 
 * here the secretary can add or remove person in a specific room.
 * This functionality hasn't been implemented yet.
 */
public class AddRemovePerson extends DocumentTemplate {
    /**
     * 
     * @param testo
     *            is the text of label
     */
    public AddRemovePerson(final String testo) {
        super(testo, StayState.INACTIVE);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void nextOperations(final Stay stay) {
        new ConfirmAddRemovePerson(stay);
    }
}