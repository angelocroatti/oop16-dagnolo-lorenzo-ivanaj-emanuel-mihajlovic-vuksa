package gui.secretary.exitdate;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.toedter.calendar.JDateChooser;

import gui.secretary.LogIn;
import gui.secretary.mainview.SceltaOpzione;
import hotelmaster.exceptions.OccupiedRoomException;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayManager;
import hotelmaster.structure.Hotel;

//CHECKSTYLE:OFF
/**
 * 
 * emanu.
 *
 */
public class ModificaDurataSoggiorno {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private JPanel northPanel;
    private JButton ok;
    private JButton esci;
    private Image okIcon;
    private Dimension screenSize;
    private Image exitIcon;
    private JDateChooser cal;
    private DateFormat nuovogg;
    private DateFormat nuovomm;
    private DateFormat nuovoaa;
    private Integer gg;
    private Integer mm;
    private Integer aa;
    private LocalDate local;
    private JLabel labelPrevisione;
    private JLabel labelNuovaData;
    private JLabel labelInfo;

    /**
     * 
     * @param documento
     */
    public ModificaDurataSoggiorno(final Stay stay) {
        this.frame = new JFrame("Modifica durata soggiorno");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.panel = new JPanel();
        this.panel.setBackground(Color.CYAN);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        this.northPanel = new JPanel();
        this.northPanel.setBackground(Color.CYAN);
        this.frame.setSize(new Dimension(400, 350));
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.nuovogg = new SimpleDateFormat("dd");
        this.nuovomm = new SimpleDateFormat("MM");
        this.nuovoaa = new SimpleDateFormat("yyyy");
        this.frame.setLocation((this.screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/ok.png")).getImage();
        this.exitIcon = new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage();
        this.labelInfo = new JLabel("" + stay.getDates().getEnd());
        this.labelPrevisione = new JLabel("Data prevista di uscita");
        this.labelNuovaData = new JLabel("Scegli nuova data");
        this.cal = new JDateChooser();
        this.cal.setDateFormatString("dd/mm/aaaa");
        this.cal.setLocale(Locale.ITALY);
        this.cal.setBackground(Color.YELLOW);
        this.cal.setDateFormatString("dd/MM/yyyy");
        this.cal.setFont(new Font("Tahoma", Font.PLAIN, 15));
        this.cal.setLocale(Locale.ITALY);
        this.cal.setPreferredSize(new Dimension(130, 20));
        this.ok = new JButton("");
        this.ok.setIcon(new ImageIcon(this.okIcon));
        this.esci = new JButton("");
        this.esci.setIcon(new ImageIcon(this.exitIcon));
        this.ok.addActionListener(a -> {

            int risp = JOptionPane.showConfirmDialog(this.frame, "Vuoi confermare la nuova data scelta?", "Conferma",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                try {
                    this.gg = Integer.parseInt(this.nuovogg.format(this.cal.getDate()));
                    this.mm = Integer.parseInt(this.nuovomm.format(this.cal.getDate()));
                    this.aa = Integer.parseInt(this.nuovoaa.format(this.cal.getDate()));
                    this.local = LocalDate.of(this.aa, Month.of(this.mm), this.gg);
                    StayManager manager = stay.getStayManager();
                    try {
                        manager.setEnd(this.local);
                    } catch (IllegalArgumentException e) {
                        JOptionPane.showMessageDialog(frame, "Inserisci una data valida", "Errore data",
                                JOptionPane.ERROR_MESSAGE);
                    } catch (OccupiedRoomException e) {
                        JOptionPane.showMessageDialog(frame, "La stanza � occupata in questo periodo", "Errore data",
                                JOptionPane.ERROR_MESSAGE);
                    }
                    this.frame.setVisible(false);
                    new SceltaOpzione();
                } catch (NullPointerException e) {
                    JOptionPane.showMessageDialog(frame, "Inserisci una data valida", "Errore data",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        this.esci.addActionListener(c -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Vuoi davvero uscire?", "Uscita",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.frame.setVisible(false);
                new LogIn();
            }
        });
        this.panel.add(this.labelPrevisione);
        this.panel.add(this.labelInfo);
        this.panel.add(this.labelNuovaData);
        this.panel.add(this.cal);
        this.southPanel.add(this.esci);
        this.southPanel.add(this.ok);
        this.frame.getContentPane().add(this.panel);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.northPanel, BorderLayout.NORTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }
}