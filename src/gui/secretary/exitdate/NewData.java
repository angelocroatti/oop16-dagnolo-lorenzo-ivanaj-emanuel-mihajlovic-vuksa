package gui.secretary.exitdate;

import gui.secretary.documentamplate.DocumentTemplate;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayState;

/**
 * 
 * emanu Modify the exit data.
 *
 */
public class NewData extends DocumentTemplate {
    /**
     * 
     * @param testo
     *            text that the secretary will read
     */
    public NewData(final String testo) {
        super(testo, StayState.INACTIVE);

    }

    @Override
    public void nextOperations(final Stay stay) {
        new ModificaDurataSoggiorno(stay);
    }
}
