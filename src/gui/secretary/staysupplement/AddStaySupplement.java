package gui.secretary.staysupplement;

import gui.secretary.documentamplate.DocumentTemplate;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayState;

/**
 * 
 * the secretary will add supplement.
 *
 */
public class AddStaySupplement extends DocumentTemplate {
    /**
     * 
     * @param testo
     *            is the texxt of label
     */
    public AddStaySupplement(final String testo) {
        super(testo, StayState.INACTIVE);
    }

    @Override
    public void nextOperations(final Stay stay) {
        new ConfirmAddSupplement(stay);
    }
}