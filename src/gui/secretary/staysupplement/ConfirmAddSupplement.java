package gui.secretary.staysupplement;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import gui.secretary.Modifica;
import gui.secretary.mainview.SceltaOpzione;
import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.pricing.StayExtraPriceDescriber;
import hotelmaster.reservations.InactiveStayManager;
import hotelmaster.reservations.Stay;
import hotelmaster.structure.Hotel;

/**
 * In this frame, will be added or removed stay extra emanu.
 *
 */
public class ConfirmAddSupplement {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private Map<JCheckBox, StayExtraPriceDescriber> map;
    private JButton conferma;
    private Dimension screenSize;
    private Stay soggiorno;

    // CHECKSTYLE:OFF: MagicNumber
    /**
     * 
     * @param document
     *            document is the number of a specific document that has a
     *            specific person who has different rooms
     */
    public ConfirmAddSupplement(final Stay stay) {
        this.frame = new JFrame();
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setSize(700, 600);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.panel = new JPanel();
        this.panel.setBackground(Color.CYAN);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        this.map = new HashMap<>();
        this.soggiorno = stay;
        for (StayExtraPriceDescriber sogg : Hotel.instance().getPriceView(StayExtraPriceDescriber.class)) {
            JCheckBox check = new JCheckBox(sogg.getDescription());
            check.setBackground(Color.cyan);
            this.panel.add(check);
            check.setSelected(soggiorno.getExtrasView().contains(sogg));
            map.put(check, sogg);
        }
        this.conferma = new JButton("");
        conferma.setIcon(new ImageIcon(this.getClass().getResource("/icons/ok.png")));
        this.conferma.addActionListener(e -> {
            InactiveStayManager manager = (InactiveStayManager) soggiorno.getStayManager();
            try {
                for (JCheckBox check : map.keySet()) {
                    if (check.isSelected()) {
                        ((InactiveStayManager) manager).addExtra(map.get(check));
                    } else {
                        ((InactiveStayManager) manager).removeExtra(map.get(check));
                    }
                }
            } catch (MissingEntityException e1) {
                JOptionPane.showMessageDialog(this.frame, "Supplemento non trovato");
            } catch (IllegalStateException e1) {
                JOptionPane.showMessageDialog(this.frame, "Errore");
            }
            JOptionPane.showMessageDialog(this.frame, "Soggiorno aggiornato", "Info", JOptionPane.PLAIN_MESSAGE);
            this.frame.setVisible(false);
            this.frame.dispose();
            new SceltaOpzione();
        });

        final JButton cancella = new JButton("");
        cancella.setIcon(new ImageIcon(this.getClass().getResource("/icons/back.png")));
        cancella.addActionListener(e -> {
            this.frame.dispose();
            new Modifica();
        });
        cancella.setVisible(true);
        southPanel.add(cancella);

        southPanel.add(conferma);
        this.conferma.setVisible(true);
        this.frame.getContentPane().add(this.panel);
        this.southPanel.add(conferma);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }

}
