package gui.secretary.pay;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import gui.secretary.mainview.SceltaOpzione;
import hotelmaster.reservations.ActiveStayManager;
import hotelmaster.reservations.Stay;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * here the secretary will read the price that a specific person will pay.
 *
 */
public class Pagamento {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private JPanel northPanel;
    private JButton conferma;
    private JButton ok;
    private JButton annulla;
    private JLabel labelPagamento;
    private Dimension screenSize;
    private Image okIcon;
    private Image exitIcon;
    private Image confirmIcon;

    public Pagamento(final Stay stay) {
        this.frame = new JFrame("Pagamento");
        this.panel = new JPanel();
        this.panel.setBackground(Color.CYAN);
        this.panel.setVisible(false);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        this.northPanel = new JPanel();
        this.northPanel.setBackground(Color.CYAN);
        this.frame.setSize(new Dimension(600, 350));
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((this.screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        Font font = new Font("Baskerville", Font.HANGING_BASELINE, 120);
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/cash.png")).getImage();
        this.confirmIcon = new ImageIcon(this.getClass().getResource("/icons/ok.png")).getImage();
        this.exitIcon = new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage();
        this.labelPagamento = new JLabel(Double.toString(stay.getTotalPrice()));
        this.labelPagamento.setFont(font);
        this.panel.add(this.labelPagamento);
        this.conferma = new JButton("");
        this.conferma.setIcon(new ImageIcon(this.okIcon));
        this.annulla = new JButton("");
        this.annulla.setIcon(new ImageIcon(this.exitIcon));
        this.ok = new JButton("");
        this.ok.setIcon(new ImageIcon(this.confirmIcon));
        this.conferma.addActionListener(a -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Vuoi confermare il pagamento ?", "Conferma",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                try {
                    ((ActiveStayManager) stay.getStayManager()).checkout();
                } catch (IllegalStateException e) {
                    JOptionPane.showMessageDialog(this.frame, "La data di fine del soggiorno non è quella odierna",
                            "Errore", JOptionPane.ERROR_MESSAGE);
                }
                this.frame.setVisible(false);
                this.frame.dispose();
                new SceltaOpzione();
            }
        });
        this.annulla.addActionListener(b -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Vuoi tornare indietro?", "Conferma",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.frame.setVisible(false);
                this.frame.dispose();
                new SceltaOpzione();
            }
        });
        this.southPanel.add(this.conferma);
        this.southPanel.add(this.annulla);
        this.frame.getContentPane().add(this.panel, BorderLayout.CENTER);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.northPanel, BorderLayout.NORTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
        this.panel.setVisible(true);
    }
}