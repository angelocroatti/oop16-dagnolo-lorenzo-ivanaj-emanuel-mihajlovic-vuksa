package gui.secretary.pay;

import gui.secretary.documentamplate.DocumentTemplate;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayState;

/**
 * 
 * here the secretary convalidate the document before payment.
 *
 */
public class PayDoc extends DocumentTemplate {
    /**
     * 
     * @param testo
     *            is the text of label
     */
    public PayDoc(final String testo) {
        super(testo, StayState.ACTIVE);
    }

    @Override
    public void nextOperations(final Stay stay) {
        new Pagamento(stay);
    }
}
