package gui.secretary.mainview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import gui.secretary.FaseRegistrazione;
import gui.secretary.LogIn;
import gui.secretary.Modifica;
import gui.secretary.pay.Pagamento;
import gui.secretary.pay.PayDoc;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * 
 *
 */
public class SceltaOpzione {
    private JFrame frame;
    private JPanel panel;
    private JButton prenotazione;
    private JButton modifica;
    private JButton pagamento;
    private JButton esci;
    private Dimension screenSize;
    private Image modifyIcon;
    private Image payIcon;
    private Image bookingIcon;
    private Image exitIcon;

    /**
     * 
     */
    public SceltaOpzione() {
        this.frame = new JFrame("Hotel Master");
        this.frame.setMinimumSize(new Dimension(800, 600));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((this.screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.panel = new JPanel(new GridLayout(4, 1));
        this.prenotazione = new JButton("ooking");
        Font font = new Font("Baskerville", Font.HANGING_BASELINE, 40);
        this.modifyIcon = new ImageIcon(this.getClass().getResource("/icons/modify48.png")).getImage();
        this.payIcon = new ImageIcon(this.getClass().getResource("/icons/payment.png")).getImage();
        this.bookingIcon = new ImageIcon(this.getClass().getResource("/icons/booking.png")).getImage();
        this.exitIcon = new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage();
        this.prenotazione.setIcon(new ImageIcon(this.bookingIcon));
        this.prenotazione.setFont(font);
        this.prenotazione.setBackground(Color.yellow);
        this.modifica = new JButton("Modify");
        this.modifica.setIcon(new ImageIcon(this.modifyIcon));
        this.modifica.setFont(font);
        this.modifica.setBackground(Color.green);
        this.pagamento = new JButton("Pay");
        this.pagamento.setIcon(new ImageIcon(this.payIcon));
        this.pagamento.setFont(font);
        this.pagamento.setBackground(Color.yellow);
        this.esci = new JButton("Exit");
        this.esci.setIcon(new ImageIcon(this.exitIcon));
        this.esci.setFont(font);
        this.esci.setBackground(Color.green);
        this.panel.add(prenotazione);
        this.panel.add(modifica);
        this.panel.add(pagamento);
        this.panel.add(this.esci);
        this.prenotazione.addActionListener(a -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new FaseRegistrazione();
        });
        this.modifica.addActionListener(c -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new Modifica();
        });
        this.pagamento.addActionListener(d -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new PayDoc("Pagamento");
        });
        this.esci.addActionListener(e -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Sei sicuro di volere uscire?", "Uscita",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.frame.setVisible(false);
                this.frame.dispose();
                new LogIn();
            }
        });
        this.frame.getContentPane().add(panel);
        this.frame.setVisible(true);
    }
}
