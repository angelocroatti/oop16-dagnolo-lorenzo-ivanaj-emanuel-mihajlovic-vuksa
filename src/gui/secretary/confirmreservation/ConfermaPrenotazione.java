package gui.secretary.confirmreservation;

import gui.secretary.documentamplate.DocumentTemplate;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayState;

//CHECKSTYLE:OFF
/**
 * 
 * the secretary confirm a specific reservation.
 *
 */
public class ConfermaPrenotazione extends DocumentTemplate {
    /**
     * 
     * @param testo
     *            is the text of label
     */
    public ConfermaPrenotazione(final String testo) {
        super(testo, StayState.INACTIVE);
    }

    @Override
    public void nextOperations(final Stay stay) {
        new ViewReservation(stay);
    }
}
