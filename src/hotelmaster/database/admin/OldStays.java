package hotelmaster.database.admin;

import hotelmaster.reservations.Stay;
/**
 * Manager for closed stays.
 */
public interface OldStays {

    /**
     * Save the main data of a closed stay.
     * @param stay the stay
     */
    void saveClosedStay(Stay stay);

}