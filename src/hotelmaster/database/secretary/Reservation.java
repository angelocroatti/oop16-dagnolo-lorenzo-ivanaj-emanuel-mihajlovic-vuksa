package hotelmaster.database.secretary;

import hotelmaster.exceptions.GuestException;
import hotelmaster.reservations.Stay;
/**
 * A stay that cannot be modified.
 */
public interface Reservation {
    /**
     * Register a new booking.
     * @param stay the new stay to be registered
     * @throws GuestException if a clients books twice.
     */
    void registerStay(Stay stay) throws GuestException;
    /**
     * Confirm a stay.
     * @param stay the stay to be registered
     */
    void confirmStay(Stay stay);
    /**
     * Close a finished stay.
     * @param stay the stay to be closed
     */
    void closeStay(Stay stay);

}