package hotelmaster.database.factory;

import hotelmaster.database.admin.PriceUtilities;
import hotelmaster.database.admin.PriceUtilitiesImpl;
import hotelmaster.database.admin.Rooms;
import hotelmaster.database.admin.RoomsImpl;
import hotelmaster.database.secretary.ModifiableReservation;
import hotelmaster.database.secretary.ModifiableReservationImpl;
import hotelmaster.database.secretary.Reservation;
import hotelmaster.database.secretary.ReservationImpl;
import hotelmaster.database.utilities.BasicReservationUtilities;
import hotelmaster.database.utilities.DocumentUtilities;
import hotelmaster.database.utilities.PersonPriceUtilities;
import hotelmaster.database.utilities.ReservationUtilities;
import hotelmaster.database.utilities.RoomExtraUtilities;
import hotelmaster.database.utilities.RoomTypeUtilities;
import hotelmaster.database.utilities.RoomUtilities;
import hotelmaster.database.utilities.SeasonUtilities;
import hotelmaster.database.utilities.StayExtraUtilities;
import hotelmaster.database.utilities.StayTypeUtilities;
/**
 * Factory for classes that interact with the database to store the data.
 */
public class DatabaseFactory implements DataFactory {

    /**
     * 
     */
    public DatabaseFactory() { }

    @Override
    public PriceUtilities getPrices() {
        return new PriceUtilitiesImpl();
    }

    @Override
    public Rooms getRooms() {
        return new RoomsImpl();
    }

    @Override
    public BasicReservationUtilities getReservations() {
        return new ReservationUtilities();
    }

    @Override
    public RoomExtraUtilities getRoomExtraUtilities() {
        return new RoomExtraUtilities();
    }

    @Override
    public RoomTypeUtilities getRoomTypeUtilities() {
        return new RoomTypeUtilities();
    }

    @Override
    public StayTypeUtilities getStayTypeUtilities() {
        return new StayTypeUtilities();
    }

    @Override
    public StayExtraUtilities getStayExtraUtilities() {
        return new StayExtraUtilities();
    }

    @Override
    public SeasonUtilities getSeasonUtilities() {
        return new SeasonUtilities();
    }

    @Override
    public DocumentUtilities getDocumentUtilities() {
        return new DocumentUtilities();
    }

    @Override
    public PersonPriceUtilities getPersonPriceUtilities() {
        return new PersonPriceUtilities();
    }

    @Override
    public RoomUtilities getRoomUtilities() {
        return new RoomUtilities();
    }

    @Override
    public ModifiableReservation getModifiableReservation() {
        return new ModifiableReservationImpl();
    }

    @Override
    public Reservation getReservation() {
        return new ReservationImpl();
    }
}
