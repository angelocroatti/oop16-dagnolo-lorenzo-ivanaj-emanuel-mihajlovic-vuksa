package hotelmaster.main;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import gui.secretary.LogIn;
import hotelmaster.database.factory.DatabaseFactory;
import hotelmaster.db.controller.DatabaseBuilder;
import hotelmaster.db.controller.DatabaseConnectionImpl;
import hotelmaster.db.controller.DemoVersion;
import hotelmaster.structure.Hotel;
//CHECKSTYLE:OFF: MagicNumbers
/**
 * The class that contains the main method, used to start the application
 */
public class Main extends Thread {

    private JFrame frame;

    /**
     * 
     */
    public Main() {
        frame = new JFrame("Scelta Demo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (DatabaseConnectionImpl.isDatabaseLoaded()) {
            Hotel.instance().setDataSource(new DatabaseFactory());
            new LogIn();
        } else {
            start();
        }
    }
    /**
     * 
     */
    @Override
    public void run() {
        JWindow window;
        window = new JWindow();
        ImageIcon img = new ImageIcon(this.getClass().getResource("/icons/patienter.gif"));
        window.getContentPane().add(new JLabel("", img, SwingConstants.CENTER));
        window.pack();
        window.setBounds(800, 300, 320, 240);
        int risp = JOptionPane.showConfirmDialog(this.frame,
                "E' la prima volta che viene avviato il programma, premere ok per caricare la demo, no per caricare una configurazione vuota ", "Caricamento Hotel",
                JOptionPane.YES_OPTION);
        window.setVisible(true);
        if (risp == JOptionPane.YES_OPTION) {
            new DatabaseBuilder().createDatabase();
            new DemoVersion().complete(); 
            Hotel.instance().setDataSource(new DatabaseFactory());
            new LogIn();
            
        } else if (risp == JOptionPane.NO_OPTION) {
            new DatabaseBuilder().createDatabase();
            new DemoVersion().basicComplete();
            Hotel.instance().setDataSource(new DatabaseFactory());
            new LogIn();
        }
        else {
            this.frame.dispose();
        }
        window.dispose();
    }

    public static void main (final String...args) {
        new Main();
    }
}
