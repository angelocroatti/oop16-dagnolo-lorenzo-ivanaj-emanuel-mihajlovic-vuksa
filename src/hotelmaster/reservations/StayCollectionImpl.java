package hotelmaster.reservations;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import hotelmaster.exceptions.GuestException;
import hotelmaster.pricing.StayExtraPriceDescriber;
import hotelmaster.pricing.StayTypePriceDescriber;
import hotelmaster.structure.ModifiableHotel;
import hotelmaster.utility.collections.Trigger;
import hotelmaster.utility.collections.TriggeringOperation;
import hotelmaster.utility.collections.TriggeringSet;
import hotelmaster.utility.time.FixedPeriod;

/**
 * Basic implementation of {@link StayCollection}.
 */
public class StayCollectionImpl implements StayCollection {

    private final Set<Stay> stays;

    StayCollectionImpl() {
        final TriggeringSet<Stay> temporary = TriggeringSet
                .from(ModifiableHotel.instance().getData().getReservations().loadStays());
        temporary.addTrigger(Trigger.create(TriggeringOperation.ADD, (stay) -> {
            try {
                ModifiableHotel.instance().getData().getReservation().registerStay(stay);
            } catch (GuestException e) {
                e.printStackTrace();
            }
        }));
        temporary.addTrigger(Trigger.create(TriggeringOperation.REMOVE, (stay) -> {
            if (stay.getState() == StayState.ACTIVE) {
                ModifiableHotel.instance().getData().getReservation().closeStay(stay);
            } else {
                ModifiableHotel.instance().getData().getModifiableReservation().deleteReservation(stay);
            }
        }));
        this.stays = Collections.synchronizedSet(temporary);
    }

    @Override
    public synchronized boolean add(final Stay e) {
        return this.stays.add(e);
    }

    @Override
    public synchronized boolean addAll(final Collection<? extends Stay> c) {
        return this.stays.addAll(c);
    }

    @Override
    public synchronized boolean contains(final Object o) {
        return this.stays.contains(o);
    }

    @Override
    public synchronized boolean containsAll(final Collection<?> c) {
        return this.stays.containsAll(c);
    }

    @Override
    public synchronized boolean isEmpty() {
        return this.stays.isEmpty();
    }

    @Override
    public synchronized Iterator<Stay> iterator() {
        return this.stays.iterator();
    }

    @Override
    public synchronized boolean remove(final Object o) {
        return this.stays.remove(o);
    }

    @Override
    public synchronized boolean removeAll(final Collection<?> c) {
        return this.stays.removeAll(c);
    }

    @Override
    public synchronized boolean retainAll(final Collection<?> c) {
        return this.stays.retainAll(c);
    }

    @Override
    public synchronized int size() {
        return this.stays.size();
    }

    @Override
    public synchronized Object[] toArray() {
        return this.stays.toArray();
    }

    @Override
    public synchronized <T> T[] toArray(final T[] a) {
        return this.stays.toArray(a);
    }

    @Override
    public synchronized void activate(final Stay stay) {
        if (this.stays.contains(stay)) {
            ((ModifiableStay) this.stays.stream().filter(s -> s.equals(stay)).findFirst().get())
                    .setState(StayState.ACTIVE);
            ModifiableHotel.instance().getData().getReservation().confirmStay(stay);
        }
    }

    @Override
    public synchronized boolean addExtra(final Stay stay, final StayExtraPriceDescriber extra) {
        if (this.stays.contains(stay)
                && ((ModifiableStay) this.stays.stream().filter(s -> s.equals(stay)).findFirst().get()).getExtras()
                        .add(extra)) {
            ModifiableHotel.instance().getData().getModifiableReservation().addExtra(stay, extra);
            return true;
        }
        return false;
    }

    @Override
    public synchronized boolean removeExtra(final Stay stay, final StayExtraPriceDescriber extra) {
        if (this.stays.contains(stay)
                && ((ModifiableStay) this.stays.stream().filter(s -> s.equals(stay)).findFirst().get()).getExtras()
                        .remove(extra)) {
            ModifiableHotel.instance().getData().getModifiableReservation().removeExtra(stay, extra);
            return true;
        }
        return false;
    }

    @Override
    public synchronized void setType(final Stay stay, final StayTypePriceDescriber type) {
        if (this.stays.contains(stay)) {
            ((ModifiableStay) this.stays.stream().filter(s -> s.equals(stay)).findFirst().get()).setType(type);
            ModifiableHotel.instance().getData().getModifiableReservation().modifyStayType(stay);
        }
    }

    @Override
    public synchronized void setDates(final Stay stay, final FixedPeriod dates) {
        if (this.stays.contains(stay)) {
            final ModifiableStay foundStay = (ModifiableStay) this.stays.stream().filter(s -> s.equals(stay))
                    .findFirst().get();
            foundStay.setDates(dates);
            ModifiableHotel.instance().getData().getModifiableReservation().modifyDates(foundStay);
        }
    }

    @Override
    public synchronized boolean addOccupation(final Stay stay, final ModifiableOccupation occupation) {
        if (this.stays.contains(stay)) {
            final ModifiableStay foundStay = (ModifiableStay) this.stays.stream().filter(s -> s.equals(stay))
                    .findFirst().get();
            if (foundStay.getOccupations().add(occupation)) {
                ModifiableHotel.instance().getData().getModifiableReservation().addRoomOccupation(foundStay,
                        occupation);
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized boolean removeOccupation(final Stay stay, final ModifiableOccupation occupation) {
        if (this.stays.contains(stay)) {
            final ModifiableStay foundStay = (ModifiableStay) this.stays.stream().filter(s -> s.equals(stay))
                    .findFirst().get();
            if (foundStay.getOccupations().remove(occupation)) {
                ModifiableHotel.instance().getData().getModifiableReservation().removeRoomOccupation(foundStay,
                        occupation);
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized boolean setOccupation(final Stay stay, final ModifiableOccupation occupation) {
        if (this.stays.contains(stay)) {
            final ModifiableStay foundStay = (ModifiableStay) this.stays.stream().filter(s -> s.equals(stay))
                    .findFirst().get();
            if (foundStay.getOccupations().contains(occupation)) {
                foundStay.getOccupations().stream().filter(occ -> occ.equals(occupation)).findFirst().get()
                        .setPeople(occupation.getPeopleView());
                ModifiableHotel.instance().getData().getModifiableReservation().modifyPeople(foundStay, occupation);
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized String toString() {
        return this.stays.toString();
    }
}
