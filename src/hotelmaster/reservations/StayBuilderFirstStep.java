package hotelmaster.reservations;

import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.pricing.StayExtraPriceDescriber;
import hotelmaster.pricing.StayTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.utility.time.FixedPeriod;

/**
 * Allows setting the client, dates, stay type and extras.
 */
public interface StayBuilderFirstStep extends StayBuilder {

    /**
     * Sets the {@link Client} of this {@link Stay} and adds the given
     * {@link Client} to the {@link Hotel}.
     * 
     * @param client
     *            the client to be set
     * @return this StayBuilder instance
     * @throws IllegalStateException
     *             the StayBuilder is already past step 1
     * @throws IllegalArgumentException
     *             the Hotel already has a stay for the given client
     */
    StayBuilderFirstStep setClient(Client client) throws IllegalStateException, IllegalArgumentException;

    /**
     * Sets the beginning and end dates of this Stay.
     * 
     * @param dates
     *            the dates to be set
     * @return this StayBuilder instance
     * @throws IllegalStateException
     *             the StayBuilder is already past step 1
     * @throws IllegalArgumentException
     *             the dates are invalid
     */
    StayBuilderFirstStep setDates(FixedPeriod dates) throws IllegalStateException, IllegalArgumentException;

    /**
     * Sets the type of this Stay.
     * 
     * @param stayType
     *            the stay type to be set.
     * @return this StayBuilder instance
     * @throws MissingEntityException
     *             the price describer does not exist in the hotel
     * @throws IllegalStateException
     *             the StayBuilder is already past step 1
     */
    StayBuilderFirstStep setType(StayTypePriceDescriber stayType) throws MissingEntityException, IllegalStateException;

    /**
     * Adds a stay extra to the already added extras.
     * 
     * @param stayExtra
     *            the extra to be added
     * @return this StayBuilder instance
     * @throws MissingEntityException
     *             the price describer does not exist in the hotel
     * @throws IllegalStateException
     *             the StayBuilder is already past step 1
     */
    StayBuilderFirstStep addExtra(StayExtraPriceDescriber stayExtra) throws MissingEntityException, IllegalStateException;

    /**
     * Retrieves the second-step staybuilder. The first step functionalities can
     * no longer be used after calling this method.
     * 
     * @return the second step of this stay builder
     * @throws IllegalStateException
     *             the first step hasn't been completed yet
     */
    StayBuilderSecondStep getSecondStep() throws IllegalStateException;
}
