package hotelmaster.reservations;

import java.util.Optional;

import hotelmaster.utility.time.FixedPeriod;

/**
 * Manages the creation of a stay through 2 steps. In the first step, the
 * client's information and the stay's duration are registerd. In the second
 * step, the rooms are added to the stay. After adding at least 1 room to the
 * stay, it may be finalized through the "complete" function. Once finalized,
 * the instance throws an exception for any method called upon it.
 */
public interface StayBuilder {

    /**
     * Returns the dates of this stay builder.
     * 
     * @return the dates
     */
    Optional<FixedPeriod> getDates();

    /**
     * Return an instanced StayBuilder.
     * 
     * @return the instance
     */
    static StayBuilderFirstStep create() {
        return new StayBuilderImpl();
    }
}
