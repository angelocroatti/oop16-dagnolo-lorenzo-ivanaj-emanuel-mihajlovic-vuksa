package hotelmaster.reservations;

import java.time.LocalDate;

import hotelmaster.structure.Hotel;

/**
 * A {@link StayManager} which allows modifying an active {@link Stay}'s state
 * and parameters.
 */
public class ActiveStayManager extends StayManager {

    ActiveStayManager(final Stay stay) throws IllegalArgumentException {
        super(stay, StayState.ACTIVE);
    }

    /**
     * Concludes the {@link Stay}, removing it from the {@link Hotel} and
     * returning its total cost. 
     * 
     * @throws IllegalStateException
     *             the stay is not active
     * 
     * @return the cost of the removed stay
     */
    public double checkout() throws IllegalStateException {
        this.checkState();
        if (!this.getStay().getDates().getEnd().equals(LocalDate.now())) {
            throw new IllegalStateException("Cannot checkout the stay: it does not end today.");
        }
        final double total = this.getStay().getTotalPrice();
        this.closeStay();
        return total;
    }
}
