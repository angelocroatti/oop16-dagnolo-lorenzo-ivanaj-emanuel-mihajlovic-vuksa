package hotelmaster.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;
import hotelmaster.structure.Room;
import hotelmaster.structure.RoomTemplate;

/**
 * Utilities for testing purposes.
 */
@SuppressWarnings("PMD.EmptyCatchBlocks")
public final class TestHelper {

    private TestHelper() {

    }

    /**
     * Creates and returns a room with any type and extra, adding it and any
     * created price describers and floors to the hotel.
     * 
     * @return the created room
     */
    public static Room createAndGetRoom() {
        // CHECKSTYLE:OFF: checkstyle:magicnumber
        final HotelManager hotelManager = HotelManager.create();
        final Room room;
        final RoomExtraPriceDescriber extraToAdd;
        final RoomTypePriceDescriber typeToSet;
        if (Hotel.instance().getPriceView(RoomExtraPriceDescriber.class).size() > 0) {
            extraToAdd = Hotel.instance().getPriceView(RoomExtraPriceDescriber.class).iterator().next();
        } else {
            extraToAdd = new RoomExtraPriceDescriber("bigwindows", 20);
            assertTrue(hotelManager.addPriceDescriber(extraToAdd));
        }
        if (Hotel.instance().getPriceView(RoomTypePriceDescriber.class).size() > 0) {
            typeToSet = Hotel.instance().getPriceView(RoomTypePriceDescriber.class).iterator().next();
        } else {
            typeToSet = new RoomTypePriceDescriber("special", 999, 0.5, 1);
            assertTrue(hotelManager.addPriceDescriber(typeToSet));
        }
        if (Hotel.instance().getRoomView().size() > 0) {
            room = Hotel.instance().getRoomView().iterator().next();
        } else {
            final RoomTemplate template = RoomTemplate.create();
            try {
                if (Hotel.instance().getPriceView(RoomTypePriceDescriber.class).size() > 0) {
                    template.setRoomType(Hotel.instance().getPriceView(RoomTypePriceDescriber.class).iterator().next());
                } else {
                    final RoomTypePriceDescriber suite = new RoomTypePriceDescriber("suite", 140, 0.5, 6);
                    assertTrue(hotelManager.addPriceDescriber(suite));
                    template.setRoomType(suite);
                }
            } catch (MissingEntityException e) {
                fail("Failed setting the room's type");
            }
            int floor = 0;
            if (Hotel.instance().getFloorView().isEmpty()) {
                hotelManager.addFloors(1);
            } else {
                floor = Hotel.instance().getFloorView().keySet().iterator().next();
            }
            try {
                hotelManager.addRooms(floor, 1, template);
            } catch (MissingEntityException e) {
                fail("Failed adding the room");
            } catch (IllegalArgumentException e) {
                fail("Failed adding the room");
            }
            room = Hotel.instance().getRoomView().iterator().next();
        }
        return room;
        // CHECKSTYLE:ON: checkstyle:magicnumber
    }

}
