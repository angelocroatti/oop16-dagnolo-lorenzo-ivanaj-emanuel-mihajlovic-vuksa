package hotelmaster.test.efficiency;

import org.junit.Before;
import org.junit.Test;

import hotelmaster.database.factory.EmptyDataFactory;
import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;
import hotelmaster.structure.Room;
import hotelmaster.structure.RoomTemplate;

/**
 * 
 */
public class FullIDs {

    /**
     * 
     */
    @Before
    public void createRooms() {
        Hotel.instance().setDataSource(new EmptyDataFactory());
        final int floors = 20;
        final int rooms = 300;
        final RoomTypePriceDescriber type = new RoomTypePriceDescriber("dfsin", 5, 5, 5);
        HotelManager.create().addPriceDescriber(type);
        final RoomTemplate template = RoomTemplate.create();
        try {
            template.setRoomType(type);
        } catch (MissingEntityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        HotelManager.create().addFloors(floors);
        int i = 0;
        while (i < floors) {
            try {
                HotelManager.create().addRooms(i, rooms, template);
            } catch (MissingEntityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            i++;
        }
        System.out.println(
                "Created " + rooms + " rooms on each floor and " + floors + " floors, total rooms = " + floors * rooms);
    }

    /**
     * 
     */
    @Test
    public void getFullIDs() {
        for (final Room r : Hotel.instance().getRoomView()) {
            r.getID().getFullID();
        }
    }
}
