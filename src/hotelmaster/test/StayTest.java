package hotelmaster.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.exceptions.OccupiedRoomException;
import hotelmaster.exceptions.PriceDescriberRemovalException;
import hotelmaster.exceptions.RoomRemovalException;
import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.pricing.StayExtraPriceDescriber;
import hotelmaster.pricing.StayTypePriceDescriber;
import hotelmaster.reservations.ActiveStayManager;
import hotelmaster.reservations.Client;
import hotelmaster.reservations.InactiveStayManager;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayBuilder;
import hotelmaster.reservations.StayBuilderFirstStep;
import hotelmaster.reservations.StayBuilderSecondStep;
import hotelmaster.reservations.StayManager;
import hotelmaster.reservations.StayState;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;
import hotelmaster.structure.Room;
import hotelmaster.utility.time.FixedPeriod;

/**
 * Tests related to {@link Stay}s, {@link StayBuilder}s and
 * {@link StayManager}s.
 */
@SuppressWarnings({ "PMD.EmptyCatchBlock", "PMD.AvoidDuplicateLiterals" })
public class StayTest extends DataSourceTest {

    /**
     * If there are no rooms, creates the rooms.
     */
    @Test
    @SuppressWarnings("PMD.NcssMethodCount") // the singleton pattern forces to
                                             // put every stay-related test in
                                             // one method, since the order in
                                             // which the stays are called is
                                             // arbitrary
    public void createStay() {
        final PersonPriceDescriber person;
        final StayTypePriceDescriber type;
        final StayExtraPriceDescriber addedExtra;
        final StayExtraPriceDescriber notAddedExtra;
        if (Hotel.instance().getPriceView(PersonPriceDescriber.class).isEmpty()) {
            person = new PersonPriceDescriber("adult", 1);
            assertTrue("Should be able to add a price describer", HotelManager.create().addPriceDescriber(person));
        } else {
            person = Hotel.instance().getPriceView(PersonPriceDescriber.class).iterator().next();
        }
        if (Hotel.instance().getPriceView(StayTypePriceDescriber.class).isEmpty()) {
            type = new StayTypePriceDescriber("fullboard", 1);
            assertTrue("Should be able to add a price describer", HotelManager.create().addPriceDescriber(type));
        } else {
            type = Hotel.instance().getPriceView(StayTypePriceDescriber.class).iterator().next();
        }
        if (Hotel.instance().getPriceView(StayExtraPriceDescriber.class).isEmpty()) {
            addedExtra = new StayExtraPriceDescriber("pool", 1, false);
            assertTrue("Should be able to add a price describer", HotelManager.create().addPriceDescriber(addedExtra));
        } else {
            addedExtra = Hotel.instance().getPriceView(StayExtraPriceDescriber.class).iterator().next();
        }
        notAddedExtra = new StayExtraPriceDescriber("somerandompricename", 3, true);
        HotelManager.create().addPriceDescriber(notAddedExtra);
        final StayBuilderFirstStep firstStep = StayBuilder.create();
        final Map<PersonPriceDescriber, Integer> people = new HashMap<>();
        people.put(person, 1);
        final Client client = Client.create("dummy", "dummy", Hotel.instance().getDocuments().iterator().next(),
                "1234567890", "phoneNumber");
        firstStep.setClient(client);
        try {
            firstStep.setDates(FixedPeriod.of(LocalDate.now().minusDays(2), LocalDate.now().plusDays(2)));
            fail("Shouldn't be able to create a stay beginning earlier than today");
        } catch (IllegalArgumentException e) {
            // all good
        }
        try {
            firstStep.setDates(FixedPeriod.of(LocalDate.now(), LocalDate.now().plusDays(2)));
            firstStep.setDates(FixedPeriod.of(LocalDate.now().plusDays(2), LocalDate.now().plusDays(4)));
        } catch (IllegalArgumentException e) {
            fail("A stay can begin today or later");
        }
        try {
            firstStep.addExtra(new StayExtraPriceDescriber("somerandomgibberishname", 1, false));
            fail("The stay extra is not in the hotel, an exception should occour");
        } catch (MissingEntityException e) {
            // all good
        } catch (IllegalStateException e) {
            fail("The stay extra should be missing");
        }
        try {
            firstStep.addExtra(addedExtra);
        } catch (MissingEntityException e) {
            fail("The stay extra is in the hotel");
        } catch (IllegalStateException e) {
            fail("The stay extra should be missing");
        }
        try {
            firstStep.setType(new StayTypePriceDescriber("somerandomgibberishname", 1));
            fail("The stay type is not in the hotel, an exception should occour");
        } catch (MissingEntityException e) {
            // all good
        } catch (IllegalStateException e) {
            fail("The stay type should be missing");
        }
        try {
            firstStep.setType(type);
        } catch (MissingEntityException e) {
            fail("The stay type is in the hotel");
        } catch (IllegalStateException e) {
            fail("The stay type should be missing");
        }
        StayBuilderSecondStep secondStep = null;
        final Room room = TestHelper.createAndGetRoom();
        try {
            secondStep = firstStep.getSecondStep();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        try {
            firstStep.addExtra(notAddedExtra);
            fail("The builder should be out of phase 1");
        } catch (MissingEntityException e) {
            fail("The stay extra is in the hotel");
        } catch (IllegalStateException e) {
            // all good
        }
        try {
            secondStep.complete();
            fail("Cannot finalize with no rooms");
        } catch (IllegalStateException e) {
            // all good
        }
        try {
            secondStep.addRoom(room, people);
        } catch (Exception e) {
            fail("Should have added successfully");
        }
        assertFalse("Should not yet contain the created stay",
                Hotel.instance().getStayView().stream().anyMatch(stay -> stay.getClient().equals(client)));
        secondStep.complete();
        assertTrue("Should contain the created stay",
                Hotel.instance().getStayView().stream().anyMatch(stay -> stay.getClient().equals(client)));
        final Stay created = Hotel.instance().getStayView().stream().filter(stay -> stay.getClient().equals(client))
                .findFirst().get();
        assertSame("Stay should be inactive", created.getState(), StayState.INACTIVE);
        StayManager baseManager = created.getStayManager();
        assertTrue("StayManager should be of sub-interface InactiveStayManager, since the stay is inactive",
                baseManager instanceof InactiveStayManager);
        final InactiveStayManager inactiveManager = (InactiveStayManager) baseManager;
        try {
            assertFalse(inactiveManager.addExtra(addedExtra));
        } catch (Exception e) {
            fail("The extra exists in the hotel");
        }
        try {
            assertTrue(inactiveManager.addExtra(notAddedExtra));
        } catch (Exception e) {
            fail("The extra exists in the hotel");
        }
        assertTrue("Should contain the added stay extra", Hotel.instance().getStayView().stream()
                .anyMatch(stay -> stay.getClient().equals(client) && stay.getExtrasView().contains(notAddedExtra)));
        try {
            inactiveManager.setBeginning(LocalDate.now());
        } catch (Exception e) {
            fail("Should be able to set the date");
        }
        inactiveManager.checkin();
        assertSame("Stay should be active", created.getState(), StayState.ACTIVE);
        try {
            inactiveManager.setBeginning(LocalDate.now().plusDays(1));
            fail("Shouldn't be able to set the date, the stay should be active");
        } catch (Exception e) {
            // all good
        }
        try {
            HotelManager.create()
                    .removeFloor(created.getOccupationsView().iterator().next().getRoom().getID().getFloor());
            fail("Should throw RoomRemovalException");
        } catch (RoomRemovalException e1) {
            // all good
        } catch (IllegalArgumentException e1) {
            fail("Should throw RoomRemovalException");
        }
        try {
            HotelManager.create().removePriceDescriber(type);
            fail("Shouldn't remove PriceDescribers in use");
        } catch (PriceDescriberRemovalException e1) {
            // all good
        }
        try {
            HotelManager.create().removePriceDescriber(person);
            fail("Shouldn't remove PriceDescribers in use");
        } catch (PriceDescriberRemovalException e1) {
            // all good
        }
        try {
            HotelManager.create().removePriceDescriber(addedExtra);
            fail("Shouldn't remove PriceDescribers in use");
        } catch (PriceDescriberRemovalException e1) {
            // all good
        }
        try {
            HotelManager.create().removePriceDescriber(notAddedExtra);
            fail("Shouldn't remove PriceDescribers in use");
        } catch (PriceDescriberRemovalException e1) {
            // all good
        }
        baseManager = created.getStayManager();
        assertTrue("StayManager should be of sub-interface ActiveStayManager, since the stay is active",
                baseManager instanceof ActiveStayManager);
        final ActiveStayManager activeManager = (ActiveStayManager) baseManager;
        LocalDate newEnd = LocalDate.now().minusDays(3);
        try {
            activeManager.setEnd(newEnd);
            fail("Set end date cannot be before today");
        } catch (IllegalArgumentException e) {
            // all good
        } catch (OccupiedRoomException e) {
            fail("Room should be free");
        }
        newEnd = LocalDate.now().plusDays(10);
        try {
            activeManager.setEnd(newEnd);
        } catch (Exception e) {
            fail("Should be possible to set the end date to a future date");
        }
        assertEquals("Date should be equal to the one just set", LocalDate.now().plusDays(10),
                created.getDates().getEnd());

        try {
            activeManager.setEnd(LocalDate.now());
            fail("Should have failed");
        } catch (IllegalArgumentException e1) {
            // all good
        } catch (OccupiedRoomException e1) {
            fail("Should have failed");
        } catch (IllegalStateException e1) {
            fail("Should have failed");
        }
        try {
            activeManager.checkout();
            fail("Cannot end a stay which does not end today");
        } catch (Exception e) {
            // all good
        }

        try {
            activeManager.setEnd(newEnd.plusDays(3));
        } catch (IllegalArgumentException | OccupiedRoomException e) {
            fail("Should throw IllegalStateException");
        } catch (IllegalStateException e) {
            // all good
        }
    }
}
