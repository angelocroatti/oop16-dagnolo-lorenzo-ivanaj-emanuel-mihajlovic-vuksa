package hotelmaster.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.exceptions.PriceDescriberRemovalException;
import hotelmaster.exceptions.RoomRemovalException;
import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;
import hotelmaster.structure.Room;
import hotelmaster.structure.RoomTemplate;

/**
 * Tests related to room price describers.
 */
@SuppressWarnings({ "PMD.EmptyCatchBlock", "PMD.AvoidDuplicateLiterals" })
public class HotelManagerTest extends DataSourceTest {

    /**
     * 
     */
    @Test
    public void addPriceDescriber() {
        final PersonPriceDescriber elder = new PersonPriceDescriber("elder", 30);
        final HotelManager hotelManager = HotelManager.create();
        assertFalse("Hotel should not contain the price describer", Hotel.instance().hasPriceDescriber(elder));
        assertTrue("Should be possible to add a price describer", hotelManager.addPriceDescriber(elder));
        assertTrue("Price describer should have been added", Hotel.instance().hasPriceDescriber(elder));
        assertFalse("Price describer should not be addable again", hotelManager.addPriceDescriber(elder));
    }

    /**
     * 
     */
    @Test
    public void removePriceDescriber() {
        final PersonPriceDescriber child = new PersonPriceDescriber("child", 20);
        final HotelManager hotelManager = HotelManager.create();
        assertFalse("Price describer is not in the hotel", Hotel.instance().hasPriceDescriber(child));
        assertTrue("Should be possible to add a price describer", hotelManager.addPriceDescriber(child));
        assertTrue("Price describer should have been added", Hotel.instance().hasPriceDescriber(child));
        try {
            assertTrue("Price describer should have been removed", hotelManager.removePriceDescriber(child));
        } catch (PriceDescriberRemovalException e) {
            fail("The price is not in use");
        }
        assertFalse("Price describer should have been removed", Hotel.instance().hasPriceDescriber(child));
    }

    /**
     * 
     */
    @Test
    public void templateNonExistingPriceDescriber() {
        final RoomExtraPriceDescriber fridge = new RoomExtraPriceDescriber("fridge", 20);
        final RoomTemplate template = RoomTemplate.create();
        try {
            template.addRoomExtra(fridge);
            fail("Should have thrown a MissingEntityException");
        } catch (MissingEntityException e) {
            // all good
        } catch (IllegalArgumentException e) {
            fail("The PriceDescriber is not in the hotel");
        }
    }

    /**
     * 
     */
    @Test
    public void roomTemplateSetup() {
        final HotelManager hotelManager = HotelManager.create();
        final RoomTypePriceDescriber singleRoom = new RoomTypePriceDescriber("single", 30, 0.5, 1);
        final RoomExtraPriceDescriber bathtub = new RoomExtraPriceDescriber("bathtub", 20);
        final RoomTemplate template = RoomTemplate.create();
        hotelManager.addPriceDescriber(singleRoom);
        try {
            template.setRoomType(singleRoom);
        } catch (MissingEntityException e) {
            fail("The entity exists in the hotel; no exception should have been thrown");
        }
        hotelManager.addPriceDescriber(bathtub);
        try {
            template.addRoomExtra(bathtub).setRoomType(singleRoom);
        } catch (MissingEntityException e) {
            fail("Adding existing price describers to a template should be possible");
        } catch (IllegalArgumentException e) {
            fail("The room extra hasn't been added yet");
        }
        try {
            template.addRoomExtra(bathtub);
            fail("The template should have thrown an exception: the room extra has already been added");
        } catch (MissingEntityException e) {
            fail("Adding existing price describers to a template should be possible");
        } catch (IllegalArgumentException e) {
            // should go here
        }
    }

    /**
     * 
     */
    @Test
    public void addAndRemoveRooms() {
        final HotelManager hotelManager = HotelManager.create();
        final RoomTypePriceDescriber bunkbeds = new RoomTypePriceDescriber("bunkbeds", 60, 0.5, 4);
        final RoomExtraPriceDescriber bigtv = new RoomExtraPriceDescriber("bigtv", 15);
        final RoomTemplate template = RoomTemplate.create();
        hotelManager.addPriceDescriber(bunkbeds);
        hotelManager.addPriceDescriber(bigtv);
        try {
            template.addRoomExtra(bigtv);
            template.setRoomType(bunkbeds);
        } catch (MissingEntityException e1) {
            fail("Should have added the price describers");
        } catch (IllegalArgumentException e1) {
            fail("Should have added the price describers");
        }
        final int previousFloors = Hotel.instance().getFloorView().size();
        int previousRooms = Hotel.instance().getRoomView().size();
        assertEquals("Should now have one more floor", previousFloors + 1, hotelManager.addFloors(1));
        try {
            hotelManager.addRooms(0, 0, template);
            fail("Shouldn't be able to \"add\" 0 or less rooms");
        } catch (MissingEntityException e) {
            fail("Should have all price describers");
        } catch (IllegalArgumentException e) {
        }
        try {
            hotelManager.addRooms(previousFloors + 2, 2, template);
            fail("Shouldn't be able to add rooms on a non-existing floor");
        } catch (MissingEntityException e) {
            fail("Should have all price describers");
        } catch (IllegalArgumentException e) {
        }
        try {
            hotelManager.addRooms(0, 2, template);
        } catch (MissingEntityException e) {
            fail("Should have all price describers");
        } catch (IllegalArgumentException e) {
            fail("Operation should have completed successfully");
        }
        assertEquals("Should have " + (int) (previousRooms + 2) + " rooms in the hotel", previousRooms + 2,
                Hotel.instance().getRoomView().size());
        try {
            assertEquals("Should now have " + previousFloors + " floors", previousFloors, hotelManager.removeFloors(1));
        } catch (RoomRemovalException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        previousRooms = Hotel.instance().getRoomView().size();
        final int floors = 3;
        final int roomsPerFloor = 15;
        int i = 0;
        assertEquals("Should now have " + floors + " more floors than before", previousFloors + floors,
                hotelManager.addFloors(3));
        while (i < floors) {
            try {
                hotelManager.addRooms(i, roomsPerFloor, template);
            } catch (MissingEntityException e) {
                fail("Should have added the rooms");
            } catch (IllegalArgumentException e) {
                fail("Should have added the rooms");
            }
            i++;
        }
        assertSame("getFloorView should return the valid representation of a floor's maximum room number",
                Hotel.instance().getRoomView().stream().filter(room -> room.getID().getFloor() == floors - 1)
                        .mapToInt(room -> room.getID().getNumberOnFloor()).max().orElse(0),
                Hotel.instance().getFloorView().get(floors - 1));
        final Set<Room> roomSnapshot = new HashSet<>(Hotel.instance().getRoomView());
        final Room someRoom = Hotel.instance().getRoomView().iterator().next();
        assertEquals("Incorrect amount of total rooms", previousRooms + floors * roomsPerFloor,
                Hotel.instance().getRoomView().size());
        try {
            hotelManager.removeRoom(someRoom);
        } catch (RoomRemovalException e) {
            fail("Room is empty, therefore it should be removable");
        } catch (IllegalArgumentException e) {
            fail("The room must be in the hotel");
        }
        assertFalse("The hotel can't contain the room which has just been removed",
                Hotel.instance().getRoomView().contains(someRoom));
        try {
            hotelManager.addRooms(someRoom.getID().getFloor(), 1, template);
        } catch (MissingEntityException e) {
            fail("The hotel contains all of the template's properties");
        } catch (IllegalArgumentException e) {
            fail("The arguments are valid");
        }
        assertTrue("The snapshot and the hotel's rooms must be the same",
                Hotel.instance().getRoomView().containsAll(roomSnapshot));
    }
}
