package hotelmaster.test;

import org.junit.BeforeClass;
import org.junit.Ignore;

import hotelmaster.database.factory.EmptyDataFactory;
import hotelmaster.structure.Hotel;

/**
 * 
 */

@SuppressWarnings("PMD")
@Ignore
public class DataSourceTest {

    /**
     * 
     */
    @BeforeClass
    public static void setDataSource() {
        try {
            Hotel.instance().setDataSource(new EmptyDataFactory());
        } catch (Exception e) {
            // all good
        }
    }

    /**
     * 
     */
    public void goAwayWarning() {
        return;
    }
}
