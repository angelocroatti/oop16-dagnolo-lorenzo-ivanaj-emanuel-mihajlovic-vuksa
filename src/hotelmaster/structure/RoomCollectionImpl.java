package hotelmaster.structure;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import hotelmaster.exceptions.RoomRemovalException;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.utility.collections.Trigger;
import hotelmaster.utility.collections.TriggeringOperation;
import hotelmaster.utility.collections.TriggeringSet;

/**
 * Basic implementation of {@link RoomCollection}.
 */
public class RoomCollectionImpl implements RoomCollection {

    private final TriggeringSet<Room> rooms;

    RoomCollectionImpl() {
        this.rooms = TriggeringSet.from(ModifiableHotel.instance().getData().getRoomUtilities().getAll());
        this.rooms.addTrigger(Trigger.create(TriggeringOperation.ADD, room -> {
            try {
                ModifiableHotel.instance().getData().getRooms().createRoom(room);
            } catch (RoomRemovalException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }));
        this.rooms.addTrigger(Trigger.create(TriggeringOperation.REMOVE, room -> {
            try {
                ModifiableHotel.instance().getData().getRooms().removeRoom(room);
            } catch (RoomRemovalException e) {
                e.printStackTrace();
            }
        }));

    }

    @Override
    public boolean add(final Room e) {
        return this.rooms.add(e);
    }

    @Override
    public boolean addAll(final Collection<? extends Room> c) {
        return this.rooms.addAll(c);
    }

    @Override
    public boolean contains(final Object o) {
        return this.rooms.contains(o);
    }

    @Override
    public boolean containsAll(final Collection<?> c) {
        return this.rooms.containsAll(c);
    }

    @Override
    public boolean isEmpty() {
        return this.rooms.isEmpty();
    }

    @Override
    public Iterator<Room> iterator() {
        return this.rooms.stream().map(room -> (Room) room).iterator();
    }

    @Override
    public boolean remove(final Object o) {
        return this.rooms.remove(o);
    }

    @Override
    public boolean removeAll(final Collection<?> c) {
        return this.rooms.removeAll(c);
    }

    @Override
    public boolean retainAll(final Collection<?> c) {
        return this.rooms.retainAll(c);
    }

    @Override
    public int size() {
        return this.rooms.size();
    }

    @Override
    public Object[] toArray() {
        return this.rooms.toArray();
    }

    @Override
    public <T> T[] toArray(final T[] a) {
        return this.rooms.toArray(a);
    }

    @Override
    public void addTrigger(final Trigger<Room> trigger) throws IllegalArgumentException {
        this.rooms.addTrigger(trigger);
    }

    @Override
    public boolean setType(final Room room, final RoomTypePriceDescriber type) {
        if (this.rooms.contains(room)) {
            final ModifiableRoom found = (ModifiableRoom) this.rooms.stream().filter(r -> room.equals(r)).findFirst()
                    .get();
            found.setType(type);
            ModifiableHotel.instance().getData().getRooms().modifyTypeOfRoom(found);
            return true;
        }
        return false;
    }

    @Override
    public boolean addExtra(final Room room, final RoomExtraPriceDescriber extra) {
        if (this.rooms.contains(room) && this.rooms.stream().filter(r -> r.equals(room)).map(r -> (ModifiableRoom) r)
                .findFirst().get().getExtras().add(extra)) {
            ModifiableHotel.instance().getData().getRooms().addExtra(room, extra);
            return true;
        }
        return false;
    }

    @Override
    public boolean removeExtra(final Room room, final RoomExtraPriceDescriber extra) {
        if (this.rooms.contains(room) && this.rooms.stream().filter(r -> r.equals(room)).map(r -> (ModifiableRoom) r)
                .findFirst().get().getExtras().remove(extra)) {
            try {
                ModifiableHotel.instance().getData().getRooms().removeExtra(room, extra);
            } catch (RoomRemovalException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    @Override
    public void clearExtras(final Room room) {
        if (this.rooms.contains(room)) {
            final ModifiableRoom foundRoom = (ModifiableRoom) this.rooms.stream().filter(r -> r.equals(room))
                    .findFirst().get();
            final Set<RoomExtraPriceDescriber> toRemove = new HashSet<>();
            for (final RoomExtraPriceDescriber extra : foundRoom.getExtras()) {
                try {
                    ModifiableHotel.instance().getData().getRooms().removeExtra(room, extra);
                    toRemove.add(extra);
                } catch (RoomRemovalException e) {
                    e.printStackTrace();
                }
            }
            foundRoom.getExtras().removeAll(toRemove);
        }
    }

    @Override
    public String toString() {
        return this.rooms.toString();
    }

}
