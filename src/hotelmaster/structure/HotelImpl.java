package hotelmaster.structure;

import java.time.LocalTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;

import hotelmaster.database.factory.DataFactory;
import hotelmaster.pricing.PriceCollection;
import hotelmaster.pricing.PriceDescriber;
import hotelmaster.reservations.Client;
import hotelmaster.reservations.DocumentType;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayCleanup;
import hotelmaster.reservations.StayCollection;
import hotelmaster.utility.collections.Trigger;
import hotelmaster.utility.collections.TriggeringOperation;

/**
 * The implementation of Hotel.
 */
public class HotelImpl implements ModifiableHotel {

    private PriceCollection prices;
    private RoomCollection rooms;
    private StayCollection stays;
    private Set<DocumentType> documents;

    private StayCleanup stayCleanup;
    private final Map<Integer, Integer> floors;

    private DataFactory data;

    static class SingletonHolder {
        private static final HotelImpl SINGLETON = new HotelImpl();
    }

    HotelImpl() {
        this.floors = new HashMap<>();
    }

    private void loadData() {
        this.prices = PriceCollection.create();
        this.rooms = RoomCollection.create();
        this.stays = StayCollection.create();
        this.documents = new HashSet<>(data.getDocumentUtilities().getAll());
        for (final int floor : this.rooms.stream().mapToInt(room -> room.getID().getFloor()).distinct().boxed()
                .collect(Collectors.toList())) {
            this.floors.put(floor, this.rooms.stream().filter(room -> room.getID().getFloor() == floor)
                    .mapToInt(room -> room.getID().getNumberOnFloor()).max().orElse(0));
        }
        triggerBinding();
        StayCleanup.call();
    }

    private void triggerBinding() {
        this.rooms.addTrigger(Trigger.create(TriggeringOperation.ADD, new Consumer<Room>() {

            @Override
            public void accept(final Room addedRoom) {
                if (floors.get(addedRoom.getID().getFloor()) < addedRoom.getID().getNumberOnFloor()) {
                    floors.put(addedRoom.getID().getFloor(), addedRoom.getID().getNumberOnFloor());
                }
            }

        }));
        this.rooms.addTrigger(Trigger.create(TriggeringOperation.REMOVE, new Consumer<Room>() {

            @Override
            public void accept(final Room removedRoom) {
                if (floors.get(removedRoom.getID().getFloor()) == removedRoom.getID().getNumberOnFloor()) {
                    floors.put(removedRoom.getID().getFloor(),
                            rooms.stream().filter(room -> room.getID().getFloor() == removedRoom.getID().getFloor())
                                    .mapToInt(room -> room.getID().getNumberOnFloor()).max().orElse(0));
                }
            }

        }));
    }

    @Override
    public Map<Integer, Integer> getFloorView() {
        return Collections.unmodifiableMap(this.floors);
    }

    @Override
    public Set<DocumentType> getDocuments() {
        return ImmutableSet.copyOf(this.documents);
    }

    @Override
    public <T extends PriceDescriber> Set<T> getPriceView(final Class<T> priceType) {
        return ImmutableSet.copyOf(this.prices.get(priceType));
    }

    @Override
    public <T extends PriceDescriber> boolean hasPriceDescriber(final T price) {
        return this.prices.contains(price);
    }

    @Override
    public Set<Room> getRoomView() {
        return ImmutableSet.copyOf(this.rooms);
    }

    @Override
    public Set<Stay> getStayView() {
        return ImmutableSet.copyOf(this.stays);
    }

    @Override
    public Set<Client> getClientView() {
        return this.stays.stream().map(stay -> stay.getClient()).collect(Collectors.toSet());
    }

    @Override
    public int addFloors(final int amount) {
        int toAdd = amount;
        while (toAdd > 0) {
            int i = 0;
            while (this.floors.containsKey(i)) {
                i++;
            }
            this.floors.put(i, 0);
            toAdd--;
        }
        return this.floors.size();
    }

    @Override
    public int removeFloor(final int floorToRemove) {
        this.rooms.removeAll(this.rooms.stream().filter(room -> room.getID().getFloor() == floorToRemove)
                .collect(Collectors.toList()));
        this.floors.remove(floorToRemove);
        return this.floors.size();
    }

    @Override
    public int removeFloors(final int amount) {
        int toRemove = amount;
        while (toRemove > 0) {
            final int floorToRemove = Collections.max(this.floors.keySet());
            this.removeFloor(floorToRemove);
            toRemove--;
        }
        return this.floors.size();
    }

    @Override
    public int nextRoomOnFloor(final int floor) throws IllegalArgumentException {
        final Iterator<Integer> iter = this.rooms.stream().filter(room -> room.getID().getFloor() == floor)
                .mapToInt(room -> room.getID().getNumberOnFloor()).sorted().iterator();
        int roomNumber = 1;
        while (iter.hasNext()) {
            if (iter.next() > roomNumber) {
                return roomNumber;
            }
            roomNumber++;
        }
        return roomNumber;
    }

    @Override
    public PriceCollection getPrices() {
        return this.prices;
    }

    @Override
    public RoomCollection getRooms() {
        return this.rooms;
    }

    @Override
    public StayCollection getStays() {
        return this.stays;
    }

    @Override
    public DataFactory getData() {
        return this.data;
    }

    @Override
    public void setDataSource(final DataFactory data) throws IllegalStateException {
        if (this.data != null) {
            throw new IllegalStateException("Data source is already set");
        }
        this.data = data;
        this.loadData();
    }

    @Override
    public void setStayCleanup(final LocalTime callTime) {
        if (this.stayCleanup != null) {
            this.stayCleanup.abort();
        }
        this.stayCleanup = StayCleanup.create(callTime);
    }

    /**
     * Returns the singleton instance of Hotel.
     * 
     * @return the singleton
     */
    protected static ModifiableHotel instance() {
        return SingletonHolder.SINGLETON;
    }
}
